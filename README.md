# Single_Screen_Game_Level
 
This is a single screen game level written in Java.

## Installation
To use:
1. download the zip file
1. extract the files
1. If not already installed, use the installation file (jre-8u202-windows-x64.exe) to install the Java Runtime Environment needed to run this application.
1. run the application (game_level.exe) file

## How to Use
```
esc --------- quit/exit
m ----------- display mouse coordinates in the upper right corner of the screen
space_bar --- interact with items
w ----------- climb up the ladder
s ----------- climb down the ladder
a ----------- walk to the left
d ----------- walk to the right
```

Explore the scene and interact with any object that shows a yellow border to see what happens.

## Credits
* game engine provided by Matthew Phillips, California State University of Sacramento
* original artwork